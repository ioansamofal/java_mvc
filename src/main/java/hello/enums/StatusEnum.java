package hello.enums;

public class StatusEnum {
    final public static Integer ACTIVE = 1;
    final public static Integer DISABLE = 2;
    final public static Integer PENDING = 3;
    final public static Integer DELIVERED = 4;
    final public static Integer REJECT = 5;
}
